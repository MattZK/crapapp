import { apiUrls } from "../../config";
import { hashPassword } from "./password";

export function newPassword(formData){

    formData.password = hashPassword(formData.password);
    formData.passwordConfirm = hashPassword(formData.passwordConfirm);

    const url = window.location.href;

    let token = url.substring(url.indexOf("=") + 1, url.length);

    return fetch(`${apiUrls.mailForgotPassword}/${token}`, {
        method: "Post",
        headers: {"Content-Type": "application/json"},
        body: JSON.stringify({password: formData.password, confirmPassword: formData.passwordConfirm})
    })
}

